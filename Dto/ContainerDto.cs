using System;
using System.Collections.Generic;

namespace NrsDataroomDemo.Dto
{
    // NOTE: this is a stubbed DTO just for this implementation.
    // to see the full DTO, visit the NRS Dataroom Swagger spec: https://staging.structuredfn.com/api/swagger/
    public class ContainerDto
    {
        public Guid Id { get; set; }
        public Guid? ParentContainerId { get; set; }
        public string Name { get; set; }
        public IEnumerable<FileItemDto> FileItems { get; set; }
        public IEnumerable<ContainerDto> Containers { get; set; }
    }
}
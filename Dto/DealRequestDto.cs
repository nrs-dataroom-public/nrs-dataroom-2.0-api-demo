using System;

namespace NrsDataroomDemo.Dto
{
    // NOTE: this is a stubbed DTO just for this implementation.
    // to see the full DTO, visit the NRS Dataroom Swagger spec: https://staging.structuredfn.com/api/swagger/
    public class DealRequestDto
    {
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactEmailAddress { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactCompany { get; set; }
        public string OtherPurpose { get; set; }
        public string IssuingCompany { get; set; }
        public string ArrangingCompany { get; set; }
        public string InvoiceDelivery { get; set; }
        public DateTime? CloseDate { get; set; }
        public bool PayUpfront { get; set; }
        public string FirstInvoice { get; set; }
        public bool HasTrustee { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingEmailAddress { get; set; }
        public string BillingCompany { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string BillingAddress { get; set; }
        public object Purpose { get; set; }
        public object Sector { get; set; }
        public object[] DealAdmins { get; set; }
    }
}
using System;
using System.Text.Json.Serialization;

namespace NrsDataroomDemo.Dto
{
    // NOTE: this is a stubbed DTO just for this implementation.
    // to see the full DTO, visit the NRS Dataroom Swagger spec: https://staging.structuredfn.com/api/swagger/
    public class UserRoleBasedPermissionRequestDto
    {
        public AccessRole Role { get; set; }
        public Guid SecurableId { get; set; }
        public string TrusteeName { get; set; }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum AccessRole
    {
        Administrator,
        Editor,
        Viewer
    }
}
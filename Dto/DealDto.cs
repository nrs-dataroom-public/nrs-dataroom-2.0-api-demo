using System;

namespace NrsDataroomDemo.Dto
{
    // NOTE: this is a stubbed DTO just for this implementation.
    // to see the full DTO, visit the NRS Dataroom Swagger spec: https://staging.structuredfn.com/api/swagger/
    public class DealDto
    {
        public Guid Id { get; set; }
        public FileSecurity DefaultPdfFileSecurity { get; set; }
        public bool AllowBulkDownload { get; set; }
        public bool ShowIndex { get; set; }
        public bool DefaultFilePublication { get; set; }
        public ContainerDto Container { get; set; }
        public string Name { get; set; }
        public Guid OwningCompanyId { get; set; }
        public DateTime? EndsOn { get; set; }
        public DateTime StartsOn { get; set; }
        public DealRequestDto DealRequest { get; set; }
    }

    [Flags]
    public enum FileSecurity
    {
        Downloadable = 1,
        Printable = 1 << 1,
        Watermarked = 1 << 2
    }
}
using System;
using System.Collections.Generic;

namespace NrsDataroomDemo.Dto
{
    // NOTE: this is a stubbed DTO just for this implementation.
    // to see the full DTO, visit the NRS Dataroom Swagger spec: https://staging.structuredfn.com/api/swagger/
    public class AccountDto
    {
        public CompanyDto Company { get; set; }
    }
}
using System;

namespace NrsDataroomDemo.Dto
{
    // NOTE: this is a stubbed DTO just for this implementation.
    // to see the full DTO, visit the NRS Dataroom Swagger spec: https://staging.structuredfn.com/api/swagger/
    public class CompanyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
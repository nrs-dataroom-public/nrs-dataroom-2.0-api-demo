# NrsDataroomDemo

NrsDataroomDemo is a basic reference implementation of a NetRoadshow Dataroom 2.0 API client.

The app demonstrates some basic API functionality, such as data room creation, file and folder creation and retrieval, and the granting of user permissions.

## Building and Configuration

To build, simply execute the `dotnet build` command from the project folder.

Configuration can be tweaked in the `appsettings.json` file. Perhaps most importantly, you will need to supply either a username or email address for the `username` field, and of course a `password`. Everything else should at least work as-is.

## Running

After the application is built and configured, run it with the `dotnet run` command from the project folder.

## Other Notes

- The project contains DTO *stubs* with only the fields necessary for the provided API client examples. These are not the full DTOs available for the domain types. See the [Swagger spec](https://staging.structuredfn.com/api/swagger/ui/index) for more detail.
- From an NRS Dataroom perspective, only users with the `Data Room Creator` role are able to create data rooms.

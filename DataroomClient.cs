using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using IdentityModel.Client;
using NrsDataroomDemo.Dto;

namespace NrsDataroomDemo
{
    public class DataroomClient
    {
        private readonly HttpClient _httpClient;

        public DataroomClient(string baseAddress)
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(baseAddress),
            };
        }

        public async Task Authenticate(string idpTokenAddress, string clientId, string clientSecret, string scope)
        {
            var response = await _httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = idpTokenAddress,
                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = scope
            });
            
            if (response.IsError)
                throw new HttpRequestException(response.Error);
            
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", response.AccessToken);
        }

        public async Task<AccountDto> GetAccount() =>
            await _httpClient.GetFromJsonAsync<AccountDto>("account");

        public async Task<DealDto> CreateDataRoom(DealDto dataRoomInfo)
        {
            using var response = await _httpClient.PostAsJsonAsync("deal", dataRoomInfo);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadFromJsonAsync<DealDto>();
        }

        public async Task<IEnumerable<DealDto>> GetDataRooms() =>
            // NOTE: "summaryList" returns lightweight data room objects for speed
            await _httpClient.GetFromJsonAsync<IEnumerable<DealDto>>("deal/summaryList");

        public async Task<DealDto> GetDataRoom(Guid dataRoomId) =>
            await _httpClient.GetFromJsonAsync<DealDto>($"deal/{dataRoomId}");

        public async Task CreateUserPermission(UserRoleBasedPermissionRequestDto permissionInfo)
        {
            using var response = await _httpClient.PostAsJsonAsync("userPermission", permissionInfo);
            response.EnsureSuccessStatusCode();
        }

        public async Task<ContainerDto> GetContainer(Guid containerId) =>
            await _httpClient.GetFromJsonAsync<ContainerDto>($"container/{containerId}");

        public async Task<ContainerDto> CreateContainer(ContainerDto containerInfo)
        {
            using var response = await _httpClient.PostAsJsonAsync("container", containerInfo);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadFromJsonAsync<ContainerDto>();
        }

        public async Task<FileItemDto> CreateFileItem(Stream fileData, string fileName, Guid parentContainerId)
        {
            var content = new MultipartFormDataContent();
            content.Add(new StreamContent(fileData), fileName, fileName);

            using var response = await _httpClient.PostAsync($"fileItem?parentContainerId={parentContainerId}", content);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadFromJsonAsync<FileItemDto>();
        }

        public async Task<Guid> GetFileItemDownloadToken(Guid fileItemId)
        {
            using var response = await _httpClient.GetAsync($"fileItem/{fileItemId}/DownloadToken");
            response.EnsureSuccessStatusCode();

            // NOTE: need to trim wrapping quotes before parsing...
            return Guid.Parse((await response.Content.ReadAsStringAsync()).Trim('"'));
        }

        public async Task<Stream> GetFileItemData(Guid fileItemId, Guid downloadToken)
        {
            // NOTE: using statement intentionally missing as to not close the content stream before it's consumed
            var response = await _httpClient.GetAsync($"fileItem/{fileItemId}/Data/{downloadToken}");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStreamAsync();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NrsDataroomDemo.Dto;

namespace NrsDataroomDemo
{
    class Program
    {
        private static IConfigurationRoot _config;
        private static DataroomClient _dataroomClient;

        static async Task Main(string[] args)
        {
            _config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .Build();

            _dataroomClient = new DataroomClient(_config["baseAddress"]);

            var idpTokenAddress = _config["idpTokenAddress"];
            var clientId = _config["clientId"];
            var clientSecret = _config["clientSecret"];
            var scope = _config["scope"];

            // log in (HttpClient will handle Set-Cookie in response)
            Console.WriteLine($"Authenticating with client_id: \"{clientId}\"...");
            await _dataroomClient.Authenticate(idpTokenAddress, clientId, clientSecret, scope);

            // get logged-in user's company ID (necessary to create a data room)
            Console.Write("Getting user account... ");
            var userCompany = (await _dataroomClient.GetAccount())
                .Company;
            Console.WriteLine($"Company name: \"{userCompany.Name}\"");

            // build an example data room
            var dataRoomId = await BuildExampleDataRoom(userCompany.Id);

            // download a file from it
            await DownloadFileItemFromDataRoom(dataRoomId);

            // print the logged-in user's data room list
            Console.WriteLine("Retrieving the user's data room list...");
            var dataRooms = await _dataroomClient.GetDataRooms();

            foreach (var dataRoom in dataRooms)
                Console.WriteLine($"    {dataRoom.Name}");
        }

        private static async Task<Guid> BuildExampleDataRoom(Guid companyId)
        {
            // create a data room
            var dataRoomStartDate = DateTime.UtcNow;
            var dataRoomEndDate = dataRoomStartDate.AddDays(7);

            var dataRoomToCreate = new DealDto
            {
                Name = $"New Data Room {Guid.NewGuid()}",
                DefaultFilePublication = true,
                ShowIndex = true,
                AllowBulkDownload = true,
                OwningCompanyId = companyId,
                StartsOn = dataRoomStartDate,
                EndsOn = dataRoomEndDate,
                DefaultPdfFileSecurity = FileSecurity.Watermarked, // NOTE: bitmask
                DealRequest = new DealRequestDto
                {
                    ContactFirstName = _config.GetSection("contact")["firstName"],
                    ContactLastName = _config.GetSection("contact")["lastName"],
                    ContactEmailAddress = _config.GetSection("contact")["emailAddress"],
                    ContactPhoneNumber = _config.GetSection("contact")["phoneNumber"],
                    ContactCompany = _config.GetSection("contact")["company"],
                    OtherPurpose = "Private Company",
                    IssuingCompany = "N/A",
                    ArrangingCompany = "N/A",
                    InvoiceDelivery = "Email",
                    CloseDate = dataRoomEndDate,
                    PayUpfront = false,
                    FirstInvoice = "Net30",
                    HasTrustee = false,
                    BillingFirstName = _config.GetSection("billingContact")["firstName"],
                    BillingLastName = _config.GetSection("billingContact")["lastName"],
                    BillingEmailAddress = _config.GetSection("billingContact")["emailAddress"],
                    BillingCompany = _config.GetSection("billingContact")["company"],
                    BillingPhoneNumber = _config.GetSection("billingContact")["phoneNumber"],
                    BillingAddress = _config.GetSection("billingContact")["address"],
                    Purpose = new
                    {
                        Id = Guid.Parse("a427fce5-d347-4b6d-9582-1a7d8f7a4896"),
                        Name = "Other"
                    },
                    Sector = new
                    {
                        Id = Guid.Parse("41317ace-4637-4488-a518-a7b2bc6c86d8"),
                        Name = "N/A"
                    },
                    DealAdmins = new object[0]
                }
            };

            Console.WriteLine($"Creating data room \"{dataRoomToCreate.Name}\"...");
            var dataRoom = await _dataroomClient.CreateDataRoom(dataRoomToCreate);

            // add Data Room Admins
            var dataRoomAdminToCreate1 = new UserRoleBasedPermissionRequestDto
            {
                Role = AccessRole.Administrator,
                SecurableId = dataRoom.Id,
                TrusteeName = "dataroomadmin1@netroadshow.com" // just an email address here...
            };

            Console.WriteLine($"Adding Data Room Admin \"{dataRoomAdminToCreate1.TrusteeName}\"...");
            await _dataroomClient.CreateUserPermission(dataRoomAdminToCreate1);

            var dataRoomAdminToCreate2 = new UserRoleBasedPermissionRequestDto
            {
                Role = AccessRole.Administrator,
                SecurableId = dataRoom.Id,
                TrusteeName = "dataroomadmin2@netroadshow.com" // just an email address here...
            };

            Console.WriteLine($"Adding Data Room Admin \"{dataRoomAdminToCreate2.TrusteeName}\"...");
            await _dataroomClient.CreateUserPermission(dataRoomAdminToCreate2);

            // create a nested pair of folders, starting at the root container of the data room
            var containerToCreate1 = new ContainerDto
            {
                Name = "Parent Folder",
                ParentContainerId = dataRoom.Container.Id
            };

            Console.WriteLine($"Creating folder \"{containerToCreate1.Name}\"...");
            var container1 = await _dataroomClient.CreateContainer(containerToCreate1);

            var containerToCreate2 = new ContainerDto
            {
                Name = "Child Folder",
                ParentContainerId = container1.Id
            };

            Console.WriteLine($"Creating folder \"{containerToCreate2.Name}\"...");
            var container2 = await _dataroomClient.CreateContainer(containerToCreate2);

            // upload a file
            var fileName = _config["uploadTarget"];
            using var fileStream = new FileStream(fileName, FileMode.Open);

            Console.WriteLine($"Uploading file \"{fileName}\" to folder \"{container2.Name}\"...");
            await _dataroomClient.CreateFileItem(fileStream, fileName, container2.Id);

            return dataRoom.Id;
        }

        private static async Task DownloadFileItemFromDataRoom(Guid dataRoomId)
        {
            var dataRoom = await _dataroomClient.GetDataRoom(dataRoomId);

            // just use the first file found...
            var fileItem = (await GetAllFileItemsInHierarchy(dataRoom.Container.Id))
                .FirstOrDefault();

            if (fileItem != null)
            {
                var downloadDestination = _config["downloadDestination"];
                Console.WriteLine($"Downloading file \"{fileItem.Name}\" into local folder \"{downloadDestination}\"...");
                await DownloadFileItem(Path.Combine(downloadDestination, fileItem.Name), fileItem.Id);
            }
        }

        private static async Task<IEnumerable<FileItemDto>> GetAllFileItemsInHierarchy(Guid containerId)
        {
            var toReturn = new List<FileItemDto>();
            var container = await _dataroomClient.GetContainer(containerId);

            foreach (var curContainer in container.Containers)
                toReturn.AddRange(await GetAllFileItemsInHierarchy(curContainer.Id));

            foreach (var curFileItem in container.FileItems)
                toReturn.Add(curFileItem);

            return toReturn;
        }

        private static async Task DownloadFileItem(string path, Guid fileItemId)
        {
            // get the download token
            var downloadToken = await _dataroomClient.GetFileItemDownloadToken(fileItemId);

            // use the token to get the file data, then write it to the destination
            using var sourceStream = await _dataroomClient.GetFileItemData(fileItemId, downloadToken);
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            using var destinationStream = new FileStream(path, FileMode.Create);
            
            await sourceStream.CopyToAsync(destinationStream);
        }
    }
}
